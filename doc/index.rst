ligo-scald documentation
========================

A dynamic data visualization and monitoring tool for
gravitational-wave data.

Features
------------

* Provides a web-based dashboard for visualizing/exploring realtime and historical data.
* Streaming timeseries, heatmap and 'latest' visualizations.
* Utilities for storing and aggregating timeseries data that is accessible via HTTP API.
* Set up nagios monitoring based on thresholds or job heartbeats.
* Full integration with InfluxDB, a timeseries database, as a data backend.
* A mock database to serve fake data for testing purposes.

User's Guide
------------

.. toctree::
   :maxdepth: 2

   user/installation
   user/quickstart
   user/commands
   user/aggregator
   user/consumer
   user/reports
   user/dashboard
   user/monitoring
   user/deployment

API Reference
-------------

.. toctree::
   :maxdepth: 2

   api/api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
