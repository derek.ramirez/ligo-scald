stages:
  - dist
  - build
  - test
  - deploy

variables:
  YUM_OPTS: "-y -q --setopt=cachedir=${CI_PROJECT_DIR}/.cache/yum --setopt=keepcache=1 --disablerepo=htcondor"

# -- dist -------------------

dist:tarball:
  stage: dist
  image: python:3.6
  script:
    - python -m pip install setuptools
    - python setup.py sdist bdist_wheel
  artifacts:
    expire_in: 18h
    paths:
      - dist

# -- build ------------------

.build: &build
  stage: build
  after_script:
    - ls -l dist/
  artifacts:
    expire_in: 3h
    paths:
      - dist

#-- rpm package

build:el7:
  <<: *build
  image: igwn/software:el7
  dependencies:
    - dist:tarball
  before_script:
    - yum update ${YUM_OPTS}
    - yum install ${YUM_OPTS}
          rpm-build
          epel-rpm-macros
          python-rpm-macros
          python3-rpm-macros
          python-setuptools
          python36-setuptools
          python2-six
          python36-six
          python2-urllib3
          python36-urllib3
          python-yaml
          python3-PyYAML
  script:
    - mkdir -pv dist/el7
    - rpmbuild -tb dist/ligo-scald*.tar.gz
    - mv -v ~/rpmbuild/RPMS/*/ligo-scald-*.rpm dist/el7/
    - mv -v ~/rpmbuild/RPMS/*/python*-ligo-scald-*.rpm dist/el7/
  artifacts:
    expire_in: 18h
    paths:
      - dist/el7
  allow_failure: true

#-- deb package

.build:debian: &build_debian
  <<: *build
  dependencies:
    - dist:tarball
  before_script:
    - apt-get update -yqq
    - apt-get install -yq
          dpkg-dev
          debhelper
          dh-python
          python-all-dev
          python3-all-dev
          python-setuptools
          python3-setuptools
          python-six
          python3-six
          python-bottle 
          python3-bottle 
          python-numpy
          python3-numpy
          python-yaml 
          python3-yaml 
          python-h5py 
          python3-h5py 
          python-ligo-common
          python3-ligo-common
          python-urllib3
          python3-urllib3
          help2man
  script:
    - pushd .
    - version=$(python setup.py --version)
    - cd dist
    - tar -xf ligo-scald-${version}.tar.gz
    - cd ligo-scald-${version}
    - dpkg-buildpackage -us -uc -b
    - popd
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - dist/debian

build:debian:stretch:
  <<: *build_debian
  image: igwn/software:stretch

# -- test -------------------

.test: &test
  stage: test
  image: python
  dependencies:
    - dist:tarball
  variables:
    PYTHON: python2
  before_script:
    - ${PYTHON} -m pip install -r requirements.txt
    - ${PYTHON} -m pip install dist/ligo-scald*.tar.gz
    - ${PYTHON} -m pip install
          "pytest==3.0.*"
          "pytest-cov"
          "kafka-python"
  script:
    - export SCALDRC_PATH=etc/example_config.yml
    - ${PYTHON} -m pytest -v --doctest-modules --cov=ligo/scald --ignore build/ --ignore dist/ --ignore doc/ --ignore setup.py

test:el7-python2:
  <<: *test
  image: igwn/software:el7
  dependencies:
    - build:el7
  before_script:
    - yum ${YUM_OPTS} install
          python-pytest
          python-pytest-cov
    - yum ${YUM_OPTS} --nogpgcheck localinstall dist/el7/python2-ligo-scald-*.rpm
    - yum ${YUM_OPTS} --nogpgcheck localinstall dist/el7/ligo-scald-*.rpm
    - pip install kafka-python confluent-kafka==0.11.5
  after_script:
    - scald --help
    - scald serve --help
    - scald deploy --help
    - scald mock --help
    - scald aggregate --help
    - scald report --help
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/yum

test:el7-python3:
  <<: *test
  image: igwn/software:el7
  dependencies:
    - build:el7
  variables:
    PYTHON: python3
  before_script:
    - yum ${YUM_OPTS} install
          python-pytest
          python-pytest-cov
    - yum ${YUM_OPTS} --nogpgcheck localinstall dist/el7/python36-ligo-scald-*.rpm
    - python3 -m pip install kafka-python confluent-kafka==0.11.5
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/yum

test:debian-python2:stretch:
  <<: *test
  image: igwn/software:stretch
  dependencies:
    - build:debian:stretch
  before_script:
    - apt-get update -yqq
    - apt-get install -yq
          python-pytest
          python-pytest-cov
          python-six
          python-bottle
          python-numpy
          python-yaml
          python-h5py
          python-ligo-common
          python-urllib3
    - dpkg -i dist/python-ligo-scald_*_all.deb
    - pip install kafka-python confluent-kafka==0.11.5
  after_script:
    - scald --help
    - scald serve --help
    - scald deploy --help
    - scald mock --help
    - scald aggregate --help
    - scald report --help
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/apt

test:debian-python3:stretch:
  <<: *test
  image: igwn/software:stretch
  dependencies:
    - build:debian:stretch
  variables:
    PYTHON: python3
  before_script:
    - apt-get update -yqq
    - apt-get install -yq
          python3-pytest
          python3-pytest-cov
          python3-six
          python3-bottle
          python3-numpy
          python3-yaml
          python3-h5py
          python3-ligo-common
          python3-urllib3
    - dpkg -i dist/python3-ligo-scald_*_all.deb
    - pip3 install kafka-python confluent-kafka==0.11.5
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/apt

# -- docs -------------------

docs:
  stage: build
  image: igwn/software:el7
  before_script:
    - pip install dist/ligo-scald*.tar.gz
    - yum ${YUM_OPTS} install graphviz
    - pip install sphinx==1.7.9 sphinxcontrib-programoutput sphinx_rtd_theme
  script:
    - python -m sphinx -b html doc site -E
  artifacts:
    paths:
      - site

pages:
  stage: deploy
  dependencies:
    - docs
  script:
    - mv -v site public
  artifacts:
    paths:
      - public
  only:
    - master
    - schedules
